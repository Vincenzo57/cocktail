<?php
namespace cocktailApp\auth;

use cocktailApp\model\User;
use mf\auth\exception\AuthentificationException;
use mf\auth\Authentification;

class cocktailAuthentification extends \mf\auth\Authentification {

    const ACCESS_LEVEL_USER  = 100;   
    const ACCESS_LEVEL_ADMIN = 200;

    /* constructeur */
    public function __construct(){
        parent::__construct();
    }
    
    public function createUser($fullname, $username, $login, $pass, $email, $adresse, $cp, $ville, $tel, $level=self::ACCESS_LEVEL_USER) {
            try{

                    $userCheck = User::where('login','=',$login)->first();
                    if(isset($userCheck)){
                        if($userCheck->login == $login)
                            throw new AuthentificationException('username already exit !');
                        return false;
                    }else{
                        $newUser = new User();
                        $newUser->fullname = $fullname;
                        $newUser->username = $username;
                        $newUser->login = $login;
                        $newUser->password = password_hash($pass,PASSWORD_DEFAULT);
                        $newUser->email = $email;
                        $newUser->adresse = $adresse;
                        $newUser->cp = $cp;
                        $newUser->ville = $ville;
                        $newUser->tel = $tel;
                        $newUser->level = $level;
                        

                        $newUser->save();
                        return true;
                    }
            }catch(AuthentificationException $e){

            }
    }

    
    public function loginUser($login, $password){
                try{

                    $userCheck = User::where('login','=',$login)->first();
                    if(isset($userCheck)){
                        if($userCheck->login != $login){
                            throw new AuthentificationException('username does not exit !');
                            return false;
                        }else{
                            
                            return $this->login($login,$userCheck->password,$password,$userCheck->level);
                        }
                    }
                    
                }catch(AuthentificationException $e){

                }
    }

}