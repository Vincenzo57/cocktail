<?php

namespace cocktailApp\view;

class cocktailView extends \mf\view\AbstractView
{

    public function __construct($data)
    {
        parent::__construct($data);
    }

    protected function renderBody($selector = null)
    {
        /*
         * voire la classe AbstractView
         *
         */
        $http_req = new \mf\utils\HttpRequest();
        $res = "";
        $res .= "<header class='row'>" . $this->renderHeader();
        $res .= "<nav  class='col s12 m12 l12 row' id='nav-menu'>";
        $res .= $this->renderTopMenu();
        $res .= "</nav>";
        $res .= "</header>";
        $res .= "<section>";
        switch ($selector) {
            case 'login':
                $res .= $this->renderViewLogin();
                break;
            case 'signup':
                $res .= $this->renderViewSignup();
                break;
            case 'cocktails':
                if (isset($_SESSION['user_login'])) {
                    $res .= $this->renderCocktails();
                } else {
                    $res .= $this->renderViewLogin();
                }
                break;
            case 'Barmiton':
                if (isset($_SESSION['user_login'])) {
                    $res .= $this->renderCocktail();
                } else {
                    $res .= $this->renderViewLogin();
                }
                break;
        }
        if (isset($_SESSION['user_login']))
            $res .= "</section>";
        $res .= "<footer class='theme-backcolor1'>" . $this->renderFooter() . "</footer>";
        return $res;
    }

    private function renderHeader()
    {
        return '<meta charset="UTF-8">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <script type="text/javascript" src="slide.js"></script>';
    }

    private function renderTopMenu()
    {
        $router = new \mf\router\Router();
        $res = "";

        if (isset($_SESSION['user_login'])) {
            if ($_SESSION['access_level'] == 200) {

                $res .= "<a class='nav-item col s2 m2 l2' href='" . $router->urlFor('logout') . "'>LOGOUT</a>";
                $res .= "<a class='login-item col s2 m2 l2' > ADMIN " . $_SESSION['user_login'] . " </a>";
            } else {
                $res .= '
                        <nav class="navbar-fixed">
                            <div class="nav-wrapper">
                                <a href="#" class="brand-logo right">Barminton</a>
                                <ul id="nav-mobile" class="left hide-on-med-and-down">
                                    <li><a href="">Bienvenue: ' . $_SESSION['user_login'] . '  </a></li>
                                    <li><a href="' . $router->urlFor('cocktails') . '">Liste des Cocktails</a></li>
                                    <li><a href="">Mes recettes</a></li>
                                    <li><a href="' . $router->urlFor('logout') . '">Logout</a></li>
                                    
                                </ul>
                            </div>
                        </nav>';
            }
        } else {
            $res .= '<nav class="navbar-fixed">
            <div class="nav-wrapper">
            <a href="#" class="brand-logo right">Barminton</a>
            <ul id="nav-mobile" class="left hide-on-med-and-down">
            <li><a href="' . $router->urlFor('login') . '">LOGIN</a></li>
            <li><a href="' . $router->urlFor('signup') . '">SIGNUP</a></li>';
        }

        return $res;
    }

    private function renderViewLogin()
    {
        $router = new \mf\router\Router();


        $res = "<div class='row'>";
        $res .= "<div class='col s1 m4 l4'></div>";
        $res .= "<div class='login-page'>
        <div class='form'>
        <form class='register-form'> 
          <input class='' type='text' name='login' id='login' placeholder='Login' required /><br>" .
            "<input class='' type='password' name='password' id='password' placeholder='Password' required /><br>" .
            "<input class='' type='submit' value='Send' id='' name='send' />
            <p class='message'>Already registered? <a href='#'>Sign In</a></p>
          </form>
          <form class='login-form' method='POST' action='" . $router->urlFor('checkLogin') . "'>
          <input class='' type='text' name='login' id='login' placeholder='Login' required /><br>
          <input class='inp' type='password' name='password' id='password' placeholder='Password' required /><br>
            <input class='inp' type='submit' value='Se connecter' id='' name='send' />
            <p class='message'>Pas enregistrer? <a href='" . $router->urlFor('signup') . "'>Creer un compte</a></p>
          </form>
        </div>
      </div>";
        return $res;
        var_dump($res);
    }

    private function renderViewSignup()
    {
        $router = new \mf\router\Router();

        $res = "<div class='row'>";
        $res .= "<div class='col s1 m4 l4'></div>";
        $res .= "<form id='tweet-form' class='form col s10 m4 l4 row' method='POST' action='" . $router->urlFor('checksignup') . "'>" .
            "<input class='' type='text' name='fullname' id='fullname' placeholder='Full name' required /><br>" .
            "<input class='' type='text' name='username' id='username' placeholder='Username' required /><br>" .
            "<input class='' type='text' name='login' id='login' placeholder='Login' required /><br>" .
            "<input class='' type='password' name='password' id='password' placeholder='Password' required /><br>" .
            "<input class='' type='password' name='rpassword' id='rpassword' placeholder='Retype password' required /><br>" .
            "<input class='' type='text' name='email' id='email' placeholder='Email' required /><br>" .
            "<input class='' type='text' name='adresse' id='adresse' placeholder='Adresse' required /><br>" .
            "<input class='' type='text' name='cp' id='cp' placeholder='CP' required /><br>" .
            "<input class='' type='text' name='ville' id='ville' placeholder='Ville' required /><br>" .
            "<input class='' type='text' name='tel' id='tel' placeholder='Tel' required /><br>" .
            "<input class='' type='submit' value='Sign up' id='signup' name='Signup' /></form>";
        $res .= "<div class='col s1 m4 l4'></div>";
        $res .= "</div>";
        return $res;
    }

    private function renderCocktails()
    {
        $router = new \mf\router\Router();
        $app_root = (new \mf\utils\HttpRequest())->root;
        $res = "";
        $res .= "<h2 class=''>Liste des cocktails</h2>";
        $res .= "<div class='row'>";
        foreach ($this->data['cocktails'] as $key => $t) {
            if (isset($_SESSION['user_login']) && $_SESSION['access_level'] == 100) {
                $res .= "
                    <div class=\"col s3 m3 l3\">
                        <div class=\"card\">
                            <div class=\"card-image\">
                                <img src=\"$app_root/html/images/$t->img\">
                            </div>
                            <div class=\"card-content\">
                                <span class=\"card-title\">$t->nom</span>
                                <strong>Prix moyen:</strong>
                                <p>$t->prix &nbsp;€</p>
                            </div>
                            <div class=\"card-action\">
                                <a href='" . $router->urlFor('Barmiton', ['id' => $t->id]) . "'>Plus de détails</a>
                            </div>
                        </div>
                    </div>";
            }
        }
        $res .= "</div>";
        return $res;
    }

    private function renderCocktail()
    {
        $router = new \mf\router\Router();
        $app_root = (new \mf\utils\HttpRequest())->root;
        $res = "";
        $res .= "<div class='row'>";
        foreach ($this->data['Barmiton'] as $key => $t) {
            if (isset($_SESSION['user_login']) && $_SESSION['access_level'] == 100) {
                $res .= "
<div class=\"col s12 m12\">
        <h2 class=\"header\">$t->nom</h2>
        <div class=\"card horizontal\">
          <div class=\"card-image\">
            <img src=\"$app_root/html/images/$t->img\">
          </div>
          <div class=\"card-stacked\">
            <div class=\"card-content\">
                    <h5><strong>Temps de préparation: </strong>$t->temps min.</h5>
                    <h5><strong>Prix moyen: </strong>$t->prix €</h5>
                    <h5><strong>Taux d'alcool: </strong>$t->taux_alcool %</h5>
                    <h5>Ingrédients</h5>
                    <ul>
                        <li>Ingrédient 1</li>
                        <li>Ingrédient 1</li>
                        <li>Ingrédient 1</li>
                        <li>Ingrédient 1</li>
                        <li>Ingrédient 1</li>
                    </ul>
            </div>
            <div class=\"card-action\">
                <h5>Préparation:</h5>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae commodi dolorum facilis in minus, modi sequi tempore unde? Alias, error facilis libero possimus quasi rem sit sunt. Ea, laboriosam, officiis.</p>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae commodi dolorum facilis in minus, modi sequi tempore unde? Alias, error facilis libero possimus quasi rem sit sunt. Ea, laboriosam, officiis.</p>
            </div>
          </div>
        </div>
      </div>";
            }
        }
        $res .= "</div>";
        return $res;

    }

    private function renderFooter()
    {
        return 'La super app créée en Licence Pro &copy;2018';
    }


}

?>