<?php
namespace cocktailApp\model;
class Cocktail extends \Illuminate\Database\Eloquent\Model {

       protected $table      = 'Barmiton';
       protected $primaryKey = 'id';     
       public    $timestamps = false;  
       
       public function cocktail()
       {
            return $this->belongsTo('cocktailApp\model\User', 'id');
       }

}