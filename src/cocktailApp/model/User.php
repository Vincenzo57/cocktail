<?php
namespace cocktailApp\model;
class User extends \Illuminate\Database\Eloquent\Model {

       protected $table      = 'user';  
       protected $primaryKey = 'id';     
       public    $timestamps = false;  
       
       public function cocktail()
       {
            return $this->hasMany('cocktailApp\model\Cocktail', 'id');
       }

       
}