<?php

namespace cocktailApp\control;

use cocktailApp\model\Cocktail;


class cocktailController extends \mf\control\AbstractController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function viewCocktails()
    {
        if (isset($_GET['tri']))
            $cocktails = Cocktail::select()->orderBy('prix', $_GET['tri'])->get();
        else {
            $cocktails = Cocktail::select()->get();

            $vue = new \cocktailApp\view\cocktailView([
                'cocktails' => $cocktails
            ]);
            return $vue->render('cocktails');
        }
    }

    public function viewCocktail()
    {
        if (isset($_GET['id']))
            $cocktail = Cocktail::where('id', '=', $_GET['id'])->get();
        else {
            $cocktail = Cocktail::where('id', '=', $_GET['id'])->get();
        }
        $vue = new \cocktailApp\view\cocktailView([
            'Barmiton' => $cocktail
        ]);
        return $vue->render('Barmiton');
    }
}