<?php
namespace cocktailApp\control;

use cocktailApp\model\User;
use cocktailApp\model\Categorie;
use cocktailApp\model\Prestation;
use cocktailApp\auth\cocktailAuthentification;
use mf\router\Router;

class cocktailAdminController extends \mf\control\AbstractController {


    public function __construct(){
        parent::__construct();
    }

    //viewFormulaireLogin
    public function viewLogin(){
        $vue = new \cocktailApp\view\cocktailView('');

        return $vue->render('login');
    }

    public function logout(){
        
        $cocktailAuth = new cocktailAuthentification();

        $cocktailAuth->logout();

        $route = new Router();
        $route->executeRoute('default');
        
    }

    //checkLogin
    public function checkLogin(){

        if(isset($_POST['login'])){
            
            $cocktailAuth = new cocktailAuthentification();
            $route = new Router();

            if($cocktailAuth->loginUser($_POST['login'], $_POST['password'])){
                
                     $route->executeRoute('default');

            }else{
                $route->executeRoute('default');
            }
        }
    }

    public function signup(){
        
        $vue = new \cocktailApp\view\cocktailView('');

        return $vue->render('signup');
        
    }


    public function checksignup(){
        
            $cocktailAuth = new cocktailAuthentification();
            $route = new Router();
                
        if(isset($_POST['login'])){
            if($_POST['password'] == $_POST['rpassword']){
                $fullname = filter_input(INPUT_POST, "fullname", FILTER_DEFAULT);
                $username = filter_input(INPUT_POST, "username", FILTER_DEFAULT);
                $login = filter_input(INPUT_POST, "login", FILTER_DEFAULT);
                $password = filter_input(INPUT_POST, "password", FILTER_DEFAULT);
                $email = filter_input(INPUT_POST, "email", FILTER_DEFAULT);
                $adresse = filter_input(INPUT_POST, "adresse", FILTER_DEFAULT);
                $cp = filter_input(INPUT_POST, "cp", FILTER_DEFAULT);
                $ville = filter_input(INPUT_POST, "ville", FILTER_DEFAULT);
                $tel = filter_input(INPUT_POST, "tel", FILTER_DEFAULT);

                if($cocktailAuth->createUser($fullname, $username, $login, $password,  $email, $adresse, $cp, $ville, $tel)){
                    $route->executeRoute('login');
                }else{
                    $route->executeRoute('signup');
                }
            }else{
                $route->executeRoute('signup');
            }
                
        }
        
        
    }


}


?>