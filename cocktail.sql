-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Lun 14 Janvier 2019 à 15:12
-- Version du serveur :  5.7.24-0ubuntu0.18.04.1
-- Version de PHP :  7.2.10-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `Barmiton`
--

-- --------------------------------------------------------

--
-- Structure de la table `Barmiton`
--

CREATE TABLE `cocktail` (
  `id` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `temps` int(11) NOT NULL,
  `status` enum('validee','refusee','attente','') NOT NULL,
  `prix` float NOT NULL,
  `id_user` int(11) NOT NULL,
  `nbr_vue` int(11) NOT NULL,
  `taux_alcool` float NOT NULL,
  `img` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Barmiton`
--

INSERT INTO `cocktail` (`id`, `nom`, `temps`, `status`, `prix`, `id_user`, `nbr_vue`, `taux_alcool`, `img`) VALUES
(1, 'rhum', 10, 'validee', 15, 1, 45, 49, 'champagne.jpg'),
(2, 'vodka', 15, 'validee', 145, 1, 444, 15, 'champagne.jpg'),
(3, 'Wisky', 52, 'validee', 85, 2, 545, 12, 'boitedenuit.jpg'),
(3, 'Jager', 75, 'validee', 45, 1, 555, 5, 'bijoux.jpg'),
(5, 'Liqueur', 45, 'validee', 45, 1, 555, 48, 'boitedenuit.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `fullname` varchar(30) NOT NULL,
  `username` varchar(30) NOT NULL,
  `login` varchar(50) NOT NULL,
  `password` varchar(200) NOT NULL,
  `email` varchar(50) NOT NULL,
  `adresse` varchar(50) NOT NULL,
  `cp` int(5) NOT NULL,
  `ville` varchar(50) NOT NULL,
  `tel` varchar(50) NOT NULL,
  `level` int(11) NOT NULL,
  `inscription` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `fullname`, `username`, `login`, `password`, `email`, `adresse`, `cp`, `ville`, `tel`, `level`, `inscription`) VALUES
(18, 'Vincent', 'Cristiani', 't', '$2y$10$ezi1E16mOKqx/acZxKSTw.2kDDQw8ZILazujb0chkYiZ133cj9Fl6', 't@d.fr', '555 rr', 57222, 'Metz', '455', 100, '2019-01-14 13:29:29');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
