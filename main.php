<?php

/* pour le chargement automatique des classes dans vendor */
require_once 'vendor/autoload.php';

require_once 'src/mf/utils/ClassLoader.php';

$loader = new mf\utils\ClassLoader("src");
$loader->register();

use mf\auth\Authentification;
use cocktailApp\auth\cocktailAuthentification;
use mf\router\Router;

$init = parse_ini_file("conf/config.ini");

$config = [
    'driver'    => $init["type"],
    'host'      => $init["host"],
    'database'  => $init["name"],
    'username'  => $init["user"],
    'password'  => $init["pass"],
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '' ];

/* une instance de connexion  */
$db = new Illuminate\Database\Capsule\Manager();

$db->addConnection( $config ); /* configuration avec nos paramètres */
$db->setAsGlobal();            /* visible de tout fichier */
$db->bootEloquent();           /* établir la consnexion */

session_start();

$router = new \mf\router\Router();

//routes for
$router->addRoute('login', '/login/', '\cocktailApp\control\cocktailAdminController', 'viewLogin',cocktailAuthentification::ACCESS_LEVEL_NONE);
$router->addRoute('checkLogin', '/checkLogin/', '\cocktailApp\control\cocktailAdminController', 'checkLogin',cocktailAuthentification::ACCESS_LEVEL_NONE);
$router->addRoute('logout', '/logout/', '\cocktailApp\control\cocktailAdminController', 'logout',cocktailAuthentification::ACCESS_LEVEL_USER);
$router->addRoute('signup', '/signup/', '\cocktailApp\control\cocktailAdminController', 'signup',cocktailAuthentification::ACCESS_LEVEL_NONE);
$router->addRoute('checksignup', '/checksignup/', '\cocktailApp\control\cocktailAdminController', 'checksignup',cocktailAuthentification::ACCESS_LEVEL_NONE);
$router->addRoute('cocktails', '/cocktails/', '\cocktailApp\control\cocktailController', 'viewCocktails',cocktailAuthentification::ACCESS_LEVEL_USER);
$router->addRoute('Barmiton', '/Barmiton/', '\cocktailApp\control\cocktailController', 'viewCocktail',cocktailAuthentification::ACCESS_LEVEL_USER);

$router->setDefaultRoute('/cocktails/');


$router->run();


?>