-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  lun. 17 déc. 2018 à 15:38
-- Version du serveur :  5.7.23
-- Version de PHP :  7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `Barmiton`
--

-- --------------------------------------------------------

--
-- Structure de la table `Barmiton`
--

DROP TABLE IF EXISTS `cocktail`;
CREATE TABLE IF NOT EXISTS `cocktail` (
  `id` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `temps` int(11) NOT NULL,
  `status` enum('validee','refusee','attente','') NOT NULL,
  `prix` float NOT NULL,
  `id_user` int(11) NOT NULL,
  `nbr_vue` int(11) NOT NULL,
  `taux_alcool` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `Barmiton`
--

INSERT INTO `cocktail` (`id`, `nom`, `temps`, `status`, `prix`, `id_user`, `nbr_vue`, `taux_alcool`) VALUES
(1, 'rhum', 15, 'validee', 15, 1, 10, 15),
(2, 'Vodka', 10, 'validee', 12, 2, 15, 19),
(3, 'Jager', 10, 'validee', 12, 1, 150, 15);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(30) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(200) NOT NULL,
  `email` varchar(50) NOT NULL,
  `adresse` varchar(50) NOT NULL,
  `cp` int(5) NOT NULL,
  `ville` varchar(50) NOT NULL,
  `tel` varchar(50) NOT NULL,
  `level` int(11) NOT NULL,
  `inscription` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `fullname`, `username`, `password`, `email`, `adresse`, `cp`, `ville`, `tel`, `level`, `inscription`) VALUES
(1, 'test', 'test', '$2y$10$z/nCqpXPWPTmmBWyzgCBUOvs8hZtvQRUIqEsofjwYup0YCZXAmzly', 'd', '', 0, '', '', 100, '0000-00-00 00:00:00'),
(2, 't', 'f', '$2y$10$y5nvYl.EjYoypy0USOzjreQksxKWoQj52mjCf5UEWChx3xzLjOoNS', 'df', 'g', 512, 'g', '15', 100, '0000-00-00 00:00:00'),
(3, 'fdf', 'df', '$2y$10$38Su2f0as1f9Rscmc3nhTOmZe/jfCUCWjYFQIKJmvhhcRz12YIbqq', 'tt@h.Gr', '', 0, '', '', 100, '0000-00-00 00:00:00'),
(4, 'lol', 'lol', '$2y$10$3mdPf/dzlu4xtcNLxCGNj.4ZR0vpbqAYrbyXhxGfSoS7R3gcCmOja', 'lol@g.Fr', '', 0, '', '', 100, '0000-00-00 00:00:00'),
(5, 'bj', 'bj', '$2y$10$QGu527vjIiK2uOymoY79beMG6JCBZC91tGUFyMaCei0WdfsPa4k9m', 'bj@bj', '29 rue', 75822, 'metz', '05284487', 100, '0000-00-00 00:00:00'),
(6, 't', 't', '$2y$10$f1v5TK3iWFco/.cY/dnekOVGJV4QEILBMvTCXQQkIC64D8TcXJHAu', 't', 't', 4, 'f', '4', 100, '0000-00-00 00:00:00'),
(7, 'a', 'a', '$2y$10$Tca.JOX8CXZ85.yOTxBUIuNWKB7pVDwGb9yAtraXCcaZQr/LwRPiq', 'a@a.fr', '29 rue ', 57000, 'METZ', '0648', 100, '0000-00-00 00:00:00'),
(8, 'Vincent', 'Vincent', '$2y$10$epQavGURzyNxKVmgFzYJ6.ywQ6WeB608nu5a9SSE6hbmWaX40C.DW', 'lo@d', '25 lo', 55, 'metz', '555', 100, '2018-12-17 14:43:41');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
